FROM python:3.9

RUN pip install tensorflow==2.10.0 tensorflow-io==0.27.0
COPY . /app

CMD python /app/test.py

# Build command
# docker build --platform=linux/arm64/v8 . -t tensorflow

# Run commands
# docker run --platform=linux/arm64/v8 tensorflow
