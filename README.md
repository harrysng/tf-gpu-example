# tf-gpu-example

## Pro-tip


*Ignore everything below and just run within Docker.*. 
However, you lose GPU support with Docker on MAC. Docker only supports NVIDIA/CUDA.  
So, if you definitely need GPU support, then follow the steps below.  

---

Example project that guides through tensorflow installation on `Macbook M1 Pro (arm64)` with GPU support.  

Tensorflow is hereafter referred to as `tf`.
All instructions assume you have cloned this repository.

```sh
tf-gpu-example
|--- environment.yml
|--- Mambaforge-4.14.0-0-MacOSX-arm64.sh
|--- test.py
|--- README.md
|--- Dockerfile
```

## Minoforge vs Anaconda

Anaconda does not work with `tf` if you want GPU support. You will have to install Miniforge instead.  
Functionally, this should not impact you. Miniforge is an equaivalent of Anaconda, the difference being that Anaconda is an enterprise product ([and has licensing costs if operating at a company > 200 employees](https://www.anaconda.com/blog/anaconda-commercial-edition-faq)). On the other hand, Miniforge is an open-source community driven equivalent which fetches packages from the `conda-forge` channel by default.

Once you are done with this installation and update your `.bash_profile` or `.zshrc` files, your conda distribution will switch to Miniforge. Your current environments will stay in place and will be usable just as before. All your commands to interact with conda will also stay the same.  

## Installation

1. Back up your existing bash/zsh profile.
```
$ cp ~/.bash_profile ~/.bash_profile_backup
```

or

```sh
$ cp ~/.zshrc ~/.zshrc_backup
```

2. Check your system architecture.

```sh
$ uname -m
>>> arm64
```

If you system is not `arm64`, you will have to look for another installation file. Start here: https://github.com/conda-forge/miniforge.

3. Give permissions to installation file.

```sh
chmod +x Mambaforge-4.14.0-0-MacOSX-arm64.sh
```

4. Execute.

```sh
./Mambaforge-4.14.0-0-MacOSX-arm64.sh
```

Follow the instructions in the terminal to install.

5. Activate Miniforge

```sh
$ source /Users/$(id -un)/mambaforge/bin/activate
```

Check that python now points to mambaforge.

```sh
$ which python
>>> /Users/{your_user}/mambaforge/bin/python
```

6. Update bash/zsh profile

Make sure you got a backup in Step 1.

Run `conda init`.  
Then restart the terminal. Open your bash/zsh profile. It should now be pointing to mambaforge.

```sh
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/{your_user}/mambaforge/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/{your_user}/mambaforge/etc/profile.d/conda.sh" ]; then
        . "/Users/{your_user}/mambaforge/etc/profile.d/conda.sh"
    else
        export PATH="/Users/{your_user}/mambaforge/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
```

7. Create conda environment

```sh
$ conda env create -n tf-test -f environment.yml
```

Activate the environment

```sh
$ conda activate tf-test
```

Check that python points to the correct environment.

```sh
$ which python
>>> /Users/{your_user}/mambaforge/envs/tf-test/bin/python
```

8. Execute the test file

```sh
$ python test.py
```

The output should look something like this.  

```sh
TensorFlow version: 2.10.0
List of available devices for tensorflow:
PhysicalDevice(name='/physical_device:CPU:0', device_type='CPU')
PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU')
Metal device set to: Apple M1 Pro

systemMemory: 32.00 GB
maxCacheSize: 10.67 GB

2022-11-28 11:51:55.778616: I tensorflow/core/common_runtime/pluggable_device/pluggable_device_factory.cc:306] Could not identify NUMA node of platform GPU ID 0, defaulting to 0. Your kernel may not have been built with NUMA support.
2022-11-28 11:51:55.779324: I tensorflow/core/common_runtime/pluggable_device/pluggable_device_factory.cc:272] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 0 MB memory) -> physical PluggableDevice (device: 0, name: METAL, pci bus id: <undefined>)
2022-11-28 11:51:56.040220: W tensorflow/core/platform/profile_utils/cpu_utils.cc:128] Failed to get CPU frequency: 0 Hz
Epoch 1/3
2022-11-28 11:51:56.340464: I tensorflow/core/grappler/optimizers/custom_graph_optimizer_registry.cc:114] Plugin optimizer for device_type GPU is enabled.
1875/1875 [==============================] - 24s 13ms/step - loss: 0.2858 - accuracy: 0.9168 
Epoch 2/3
1875/1875 [==============================] - 22s 12ms/step - loss: 0.1349 - accuracy: 0.9601
Epoch 3/3
1875/1875 [==============================] - 25s 13ms/step - loss: 0.1009 - accuracy: 0.9695
2022-11-28 11:53:07.510301: I tensorflow/core/grappler/optimizers/custom_graph_optimizer_registry.cc:114] Plugin optimizer for device_type GPU is enabled.
313/313 - 2s - loss: 0.1023 - accuracy: 0.9687 - 2s/epoch - 7ms/step
```
