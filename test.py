# test.py

import tensorflow as tf
print("TensorFlow version:", tf.__version__)

print("List of available devices for tensorflow:")
for device in tf.config.list_physical_devices():
    print(f"{device}")

def test_tensorflow():
    # Load data
    mnist = tf.keras.datasets.mnist
    
    # Split
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0
    
    # Model definition
    model = tf.keras.models.Sequential([
      tf.keras.layers.Flatten(input_shape=(28, 28)),
      tf.keras.layers.Dense(128, activation='relu'),
      tf.keras.layers.Dropout(0.2),
      tf.keras.layers.Dense(10)
    ])

    # Loss function
    loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    
    # Compile model
    model.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])
    
    # Fit model
    model.fit(x_train, y_train, epochs=1)

    # Evaluate model
    model.evaluate(x_test,  y_test, verbose=2)
    return

if __name__ == "__main__":
    test_tensorflow()
